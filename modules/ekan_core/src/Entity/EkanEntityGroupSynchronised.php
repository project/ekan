<?php

namespace Drupal\ekan_core\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;

/**
 * Trait applies to resources and datasets to enable group synchronisation.
 */
trait EkanEntityGroupSynchronised {

  /**
   * Add or removes an entity from groups derived from the dataset publishers.
   */
  public function groupSynchronise() {

    $publisher_list = [];

    /** @var \Drupal\group\Plugin\GroupContentEnablerManagerInterface $group_content_enabler_manager */
    $group_content_enabler_manager = \Drupal::service('plugin.manager.group_content_enabler');
    $plugin_ids = $group_content_enabler_manager->getPluginIdsByEntityTypeAccess($this->getEntityTypeId());
    $group_content_plugin_id = reset($plugin_ids);
    if (empty($group_content_plugin_id)) {
      return;
    }

    try {
      /** @var \Drupal\group\Entity\Storage\GroupContentStorageInterface $group_content_storage */
      $group_content_storage = \Drupal::entityTypeManager()->getStorage('group_content');
      $group_storage = \Drupal::entityTypeManager()->getStorage('group');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $ex) {
      watchdog_exception('ekan_core', $ex);
      return;
    }

    // Base the group membership off the publisher on the dataset or related
    // dataset.
    if ($this->hasField('dataset_ref')) {
      $dataset = $this->get('dataset_ref')->entity;
      if ($dataset && $dataset->get('publisher') instanceof EntityReferenceFieldItemListInterface) {
        $publisher_list = $dataset->get('publisher')->referencedEntities();
      }
    }
    else {
      if ($this->get('publisher') instanceof EntityReferenceFieldItemListInterface) {
        $publisher_list = $this->get('publisher')->referencedEntities();
      }
    }

    $add_group_ids = [];

    /** @var \Drupal\group\Entity\GroupInterface $publisher */
    foreach ($publisher_list as $publisher) {
      $add_group_ids[] = $publisher->id();
    }

    // Remove memberships that no longer apply.
    $current_memberships = $group_content_storage->loadByEntity($this);
    foreach ($current_memberships as $membership) {
      $group = $membership->getGroup();
      if (!$group || !in_array($group->id(), $add_group_ids)) {
        try {
          $membership->delete();
        }
        catch (EntityStorageException $ex) {
          watchdog_exception('ekan_core', $ex);
        }
      }
    }

    // Add new memberships.
    /** @var \Drupal\group\Entity\GroupInterface $group */
    foreach ($group_storage->loadMultiple($add_group_ids) as $group) {
      $memberships = $group->getContentByEntityId($group_content_plugin_id, $this->id());
      if (empty($memberships)) {
        $group->addContent($this, $group_content_plugin_id);
      }
    }
  }

}
